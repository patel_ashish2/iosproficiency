//
//  CustomTableViewCell.h
//  iOSProficiency
//
//  Created by iOS Ashish G. Patel on 05/12/17.
//  Copyright © 2017 iOS Ashish G. Patel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTableViewCell : UITableViewCell

@property(nonatomic,strong) UILabel *lblTitle;
@property(nonatomic,strong) UILabel *lblDescription;
@property(nonatomic,strong) UIImageView *imgThumbView;

@end
