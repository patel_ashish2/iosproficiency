//
//  CustomTableViewCell.m
//  iOSProficiency
//
//  Created by iOS Ashish G. Patel on 05/12/17.
//  Copyright © 2017 iOS Ashish G. Patel. All rights reserved.
//

#import "CustomTableViewCell.h"
#import <PureLayout/PureLayout.h>
#import "Header.h"

@implementation CustomTableViewCell
@synthesize lblTitle,lblDescription,imgThumbView;

//Added contraints using third party PureLayout API
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        @try {
        // Initialization code
        imgThumbView = [[UIImageView alloc]init];
        imgThumbView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:imgThumbView];
        imgThumbView.translatesAutoresizingMaskIntoConstraints = NO;
        
        lblTitle = [[UILabel alloc]init];
        lblTitle.font = [UIFont boldSystemFontOfSize:22];
        lblTitle.backgroundColor = [UIColor clearColor];
        lblTitle.numberOfLines = 2;
        lblTitle.lineBreakMode = NSLineBreakByWordWrapping;
        [self.contentView addSubview:lblTitle];
        lblTitle.translatesAutoresizingMaskIntoConstraints = NO;

        
        lblDescription = [[UILabel alloc]init];
        lblDescription.font = [UIFont systemFontOfSize:18];
        lblDescription.backgroundColor = [UIColor clearColor];
        lblDescription.numberOfLines = 0;
        lblDescription.lineBreakMode = NSLineBreakByWordWrapping;
        [self.contentView addSubview:lblDescription];
        lblDescription.translatesAutoresizingMaskIntoConstraints = NO;
        
        static const CGFloat kSmallPadding = 10.0;
        
        [self.contentView autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self withOffset:0];
        [self.contentView autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self withOffset:0];
        [self.contentView autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self withOffset:0];
        [self.contentView autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self withOffset:0];
        [self.contentView autoSetDimension:ALDimensionHeight toSize:kCountryCellHeight relation:NSLayoutRelationGreaterThanOrEqual];

        [imgThumbView autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.contentView withOffset:kSmallPadding];
        [imgThumbView autoPinEdgeToSuperviewEdge:ALEdgeLeading withInset:kSmallPadding];
        [imgThumbView autoSetDimension:ALDimensionWidth toSize:60];
        [imgThumbView autoSetDimension:ALDimensionHeight toSize:60];
        [imgThumbView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionHeight ofView:imgThumbView];

        [NSLayoutConstraint autoSetPriority:UILayoutPriorityDefaultLow forConstraints:^{
            [self->imgThumbView autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:kSmallPadding];
        }];

        
        [lblTitle autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.contentView withOffset:kSmallPadding];
        [lblTitle autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:imgThumbView withOffset:kSmallPadding];
        [lblTitle autoPinEdgeToSuperviewEdge:ALEdgeTrailing withInset:kSmallPadding];
        
        [lblDescription autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:lblTitle withOffset:0];
        [lblDescription autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:imgThumbView withOffset:kSmallPadding];
        [lblDescription autoPinEdgeToSuperviewEdge:ALEdgeTrailing withInset:kSmallPadding];
        [lblDescription autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:kSmallPadding];
        } @catch (NSException *exception) {
            NSLog(@"%@",exception.description);
        }
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];

    // Make sure the contentView does a layout pass here so that its subviews have their frames set, which we
    [self.contentView setNeedsLayout];
    [self.contentView layoutIfNeeded];
}




@end
