//
//  CountryViewController.m
//  iOSProficiency
//
//  Created by iOS Ashish G. Patel on 05/12/17.
//  Copyright © 2017 iOS Ashish G. Patel. All rights reserved.
//

#import "CountryViewController.h"

@interface CountryViewController () {
    UIRefreshControl *refreshControl;
}
@property (nonatomic, strong, readonly) CountryViewModel *viewModel;

@end

@implementation CountryViewController

#pragma mark - Lifecycle

- (instancetype)initWithViewModel:(CountryViewModel *)viewModel {
    self = [super init];
    if (!self) return nil;
    _viewModel = viewModel;
    return self;
}

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupInitialContent];
}

//Setup tableview
-(void)setupInitialContent {
    @try {
        self.navigationItem.title = @"Loading..";
        self.view.backgroundColor = [UIColor whiteColor];
        self.tableView.estimatedRowHeight = kCountryCellHeight;
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        
        refreshControl = [[UIRefreshControl alloc]init];
        [self.tableView addSubview:refreshControl];
        [refreshControl addTarget:self action:@selector(refreshContentAction) forControlEvents:UIControlEventValueChanged];
        [self refreshContentAction];
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    }

}

-(void)viewWillAppear:(BOOL)animated {
    self.view.translatesAutoresizingMaskIntoConstraints = NO;
    [self.tableView setNeedsLayout];
    [self.tableView layoutIfNeeded];
}


#pragma mark - Bindings

//Refresh tableview content
- (void)refreshContentAction {
    @try {
        [_viewModel refreshCountryData:^(NSArray *array,NSString *title,NSError *error){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self->refreshControl endRefreshing];
            if (error) {
                UIAlertController * ErrorMessage = [UIAlertController
                                                    alertControllerWithTitle:@"Error"
                                                    message:error.userInfo.description
                                                    preferredStyle:UIAlertControllerStyleAlert];

                UIAlertAction *okAction = [UIAlertAction
                                           actionWithTitle:@"OK"
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction *action)
                                           {
                                               NSLog(@"OK action");
                                           }];

                [ErrorMessage addAction:okAction];
                [self presentViewController: ErrorMessage animated:YES completion:nil];
            }
            else {
                self.navigationItem.title=title;
                NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:0];
                [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
            }
            });
        }];
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.viewModel numberOfCountriesInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CustomTableViewCell *cell;
    @try {
        cell = [tableView dequeueReusableCellWithIdentifier:@kCountryCell];
        if (cell == nil) {
            cell = [[CustomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@kCountryCell];
        }
        [_viewModel setupCountryData:cell withIndexPath:indexPath];
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    }
    return cell;
}

//Tableview delegate methods
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kCountryCellHeight;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
}

@end
