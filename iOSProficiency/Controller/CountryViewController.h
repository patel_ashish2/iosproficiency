//
//  CountryViewController.h
//  iOSProficiency
//
//  Created by iOS Ashish G. Patel on 05/12/17.
//  Copyright © 2017 iOS Ashish G. Patel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"

@interface CountryViewController : UITableViewController

- (instancetype)initWithViewModel:(CountryViewModel *)viewModel;

@end
