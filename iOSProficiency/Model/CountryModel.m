//
//  CountryModel.m
//  iOSProficiency
//
//  Created by iOS Ashish G. Patel on 05/12/17.
//  Copyright © 2017 iOS Ashish G. Patel. All rights reserved.
//

#import "CountryModel.h"

@implementation CountryModel
- (instancetype)initWithTitle:(NSString *)title Description:(NSString *)description andImgPath:(NSString *)imgPath {
    self = [super init];
    if (!self) return nil;
    
    _strTitle = [title copy];
    _strDescription = [description copy];
    _strImgPath = [imgPath copy];
    
    return self;

}
@end
