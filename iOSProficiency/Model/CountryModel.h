//
//  CountryModel.h
//  iOSProficiency
//
//  Created by iOS Ashish G. Patel on 05/12/17.
//  Copyright © 2017 iOS Ashish G. Patel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CountryModel : NSObject
@property (nonatomic,strong) NSString *strTitle;
@property (nonatomic,strong) NSString *strDescription;
@property (nonatomic,strong) NSString *strImgPath;

- (instancetype)initWithTitle:(NSString *)title Description:(NSString *)description andImgPath:(NSString *)imgPath;

@end
