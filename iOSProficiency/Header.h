//
//  Header.h
//  iOSProficiency
//
//  Created by iOS Ashish G. Patel on 05/12/17.
//  Copyright © 2017 iOS Ashish G. Patel. All rights reserved.
//

#import "CountryModel.h"
#import "CustomTableViewCell.h"
#import "CountryModel.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>
#import "PureLayout.h"
#import "NSDictionary+Safety.h"
#import "CountryViewModel.h"
#import "CountryAPI.h"

#define kBaseURL "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json"
#define kTitle "title"
#define kRaws "rows"
#define kDescription "description"
#define kImageHref "imageHref"
#define kCountryCell "CountryCell"

#define kCountryCellHeight 80
