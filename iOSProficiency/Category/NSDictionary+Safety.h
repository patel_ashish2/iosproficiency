//
//  NSDictionary+Safety.h
//  iOSProficiency
//
//  Created by WiOS Ashish G. Patel on 05/12/17.
//  Copyright © 2017 iOS Ashish G. Patel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Safety)
- (id)safeObjectForKey:(id)aKey;

@end
