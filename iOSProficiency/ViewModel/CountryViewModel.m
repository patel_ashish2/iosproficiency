//
//  TableViewModel.m
//  iOSProficiency
//
//  Created by iOS Ashish G. Patel on 05/12/17.
//  Copyright © 2017 iOS Ashish G. Patel. All rights reserved.
//

#import "CountryViewModel.h"
#import "Header.h"
@interface CountryViewModel ()

@property (nonatomic, strong, readonly) CountryAPI *countryAPI;
@property (nonatomic, strong) NSArray *countries;

@end

@implementation CountryViewModel

- (instancetype)initWithCountryAPI:(CountryAPI *)countryAPI {
    self = [super init];
    if (!self) return nil;
    
    _countryAPI = countryAPI;

    return self;
}

-(void)refreshCountryData:(callback)callBack {
    [_countryAPI refreshCountryContentRequestWithCallback:^(NSArray * _Nonnull array, NSString * _Nonnull title, NSError * _Nonnull error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self->_countries = [array copy];
            callBack(array,title,error);
        });
    }];
}

#pragma mark - Data Source

- (NSUInteger)numberOfCountriesInSection:(NSInteger)section {
    return self.countries.count;
}

- (void)setupCountryData:(CustomTableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath {
    CountryModel *objCountryModel = [self personAtIndexPath:indexPath];
    cell.lblTitle.text= objCountryModel.strTitle;
    cell.lblDescription.text = objCountryModel.strDescription;
    [cell.imgThumbView sd_setImageWithURL:[NSURL URLWithString:objCountryModel.strImgPath]
                         placeholderImage:[UIImage imageNamed:@"placeholder"]
                                  options:SDWebImageRefreshCached];
    
    [cell.imgThumbView sd_setShowActivityIndicatorView:YES];
    [cell.imgThumbView sd_setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [cell layoutIfNeeded];
}

- (CountryModel *)personAtIndexPath:(NSIndexPath *)indexPath {
    return self.countries[indexPath.row];
}



@end
