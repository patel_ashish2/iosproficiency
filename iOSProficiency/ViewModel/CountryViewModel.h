//
//  TableViewModel.h
//  iOSProficiency
//
//  Created by iOS Ashish G. Patel on 05/12/17.
//  Copyright © 2017 iOS Ashish G. Patel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Header.h"
#import "CountryAPI.h"

@interface CountryViewModel : NSObject

- (instancetype)initWithCountryAPI:(CountryAPI *)countryAPI;

- (NSUInteger)numberOfCountriesInSection:(NSInteger)section;
-(void)refreshCountryData:(callback)callBack;
- (void)setupCountryData:(CustomTableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath;

@end
