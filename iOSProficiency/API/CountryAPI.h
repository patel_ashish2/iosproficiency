//
//  CountryAPI.h
//  iOSProficiency
//
//  Created by iOS Ashish G. Patel on 05/12/17.
//  Copyright © 2017 iOS Ashish G. Patel. All rights reserved.
//


#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
typedef void (^callback) (NSArray *_Nullable array,NSString *_Nullable title,NSError * _Nullable error);

@interface CountryAPI : NSObject

+ (instancetype)sharedAPIManager;

//  Get latest records from server.
- (void)refreshCountryContentRequestWithCallback:(callback)callback;

@end

NS_ASSUME_NONNULL_END
