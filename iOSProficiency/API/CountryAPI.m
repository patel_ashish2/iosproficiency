//
//  CountryAPI.m
//  iOSProficiency
//
//  Created by iOS Ashish G. Patel on 05/12/17.
//  Copyright © 2017 iOS Ashish G. Patel. All rights reserved.
//


#import "CountryAPI.h"
#import "Header.h"

@implementation CountryAPI
// We create a singleton shareAPIManager to handle all out API interactions.
+ (instancetype)sharedAPIManager {
    static CountryAPI* shareAPI = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shareAPI = [[CountryAPI alloc] init];
    });
    return shareAPI;
}

//  Get latest records from server. As per suggetion used NSURLConnection
- (void)refreshCountryContentRequestWithCallback:(callback)callback {
    @try {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            NSURL *url = [NSURL URLWithString:@kBaseURL];
            NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            #pragma clang diagnostic push
            #pragma clang diagnostic ignored "-Wdeprecated-declarations"
            [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *responseData, NSError *error) {
                if (error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        callback(nil,nil,error); });
                }
                else {
                    
                    NSError *e = nil;
                    NSString *iso = [[NSString alloc] initWithData:responseData encoding:NSISOLatin1StringEncoding];
                    NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                    NSDictionary *dicResponse = [NSJSONSerialization JSONObjectWithData:dutf8 options:NSJSONReadingMutableContainers error:&e];
                    if (dicResponse[@kTitle] && dicResponse[@kRaws]) {
                        NSArray* responseArray = [dicResponse objectForKey:@kRaws];
                        NSMutableArray *arr=[NSMutableArray array];
                        for (int i=0; i<responseArray.count; i++) {
                            NSDictionary *dictTemp = responseArray[i];
                            CountryModel *model=[[CountryModel alloc] init];
                            model.strTitle=[dictTemp safeObjectForKey:@kTitle];
                            model.strDescription = [dictTemp safeObjectForKey:@kDescription];
                            model.strImgPath = [dictTemp safeObjectForKey:@kImageHref];
                            [arr addObject:model];
                        }
                        dispatch_async(dispatch_get_main_queue(), ^{
                            callback(arr,[dicResponse safeObjectForKey:@kTitle],nil);
                        });
                    }
                    
                }
            }];
#pragma clang diagnostic pop
        });
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    }
    
}

@end
